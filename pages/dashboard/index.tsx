import React, { useEffect, useState } from "react";
import { ReactElement } from "react";
import { CustomHeader } from "../../components/Header/CustomHeader";
import MainAdmin from "../../components/Layouts/MainAdmin";
import LoadingPage from "../../components/Loading/LoadingPage";
import Analysis from "../../containers/Charts/Analysis";
import Area from "../../containers/Charts/Area";
import MultipleRadialbars from "../../containers/Charts/MultipleRadialbars";
import { getPurchaseAll } from "../../services/product";
import { PurchaseProps } from "../../interfaces/product";

const columnProduct = [
  "Số thứ tự",
  "Tên sản phẩm",
  // "Mô tả",
  "Size",
  "Màu sắc",
  // "Loại hàng",
  "Số lượng bán",
  // "Giới tính",
  // "Ảnh",
  "Doanh thu",
  // "Ngày tạo",
  // "",
  // "",
];

const Dashboard = ({ loading }: { loading: Boolean }) => {
  const [countProd, setCountProd] = useState(0);
  const [temporaryQuantity, setTemporaryQuantity] = useState(0);
  console.log(temporaryQuantity, "temporaryQuantity");
  const [totalPrice, setTotalPrice] = useState(0);
  const [totalEstimatedPrice, setTotalEstimatedPrice] = useState(0);

  const [selling, setSelling] = useState<any[]>([]);
  selling.reverse();

  const reverseSelling = selling.reverse();
  console.log(reverseSelling, "reverseSelling");

  console.log(selling, "sellingxxx");
  const fetchAllPurchase = async () => {
    try {
      const res = await getPurchaseAll();
      console.log("resxxx", res);
      const deliveredProducts = res.data.result.filter(
        (product: any) => product.status === "delivered"
      );
      const nonDeliveredProducts = res.data.result.filter(
        (product: any) => product.status !== "delivered"
      );
      setTemporaryQuantity(nonDeliveredProducts.length);
      setCountProd(deliveredProducts.length);
      // setCountProd(res.data.result.length);

      // setTotalPrice(
      //   res.data.result.reduce(
      //     (acc: number, cur: PurchaseProps) =>
      //       acc + cur.priceProd * cur.quantityProd,
      //     0
      //   )
      // );

      const totalEstimatedRevenue = res.data.result.reduce(
        (acc: number, cur: PurchaseProps) => {
          if (cur.status !== "delivered") {
            return acc + cur.priceProd * cur.quantityProd;
          }
          return acc;
        },
        0
      );
      setTotalEstimatedPrice(totalEstimatedRevenue);

      const totalRevenue = res.data.result.reduce(
        (acc: number, cur: PurchaseProps) => {
          if (cur.status === "delivered") {
            return acc + cur.priceProd * cur.quantityProd;
          }
          return acc;
        },
        0
      );
      setTotalPrice(totalRevenue);

      const productMap = new Map<string, any>();

      res.data.result.forEach((item: PurchaseProps) => {
        if (item.status === "delivered") {
          const key = `${item.nameProd}-${item.sizeProd}-${item.colorProd}`;
          if (productMap.has(key)) {
            const existingProduct = productMap.get(key);
            existingProduct.quantitySold += item.quantityProd;
            existingProduct.totalRevenue += item.priceProd * item.quantityProd;
          } else {
            productMap.set(key, {
              productName: item.nameProd,
              size: item.sizeProd,
              color: item.colorProd,
              quantitySold: item.quantityProd,
              totalRevenue: item.priceProd * item.quantityProd,
            });
          }
        }
      });

      const soldProducts = Array.from(productMap.values()).sort(
        (a, b) => b.quantitySold - a.quantitySold
      );
      setSelling(soldProducts);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchAllPurchase();
  }, []);
  return (
    <div className="text-white">
      {loading && <LoadingPage />}
      <CustomHeader>
        <title>DashBoard</title>
      </CustomHeader>
      <div className="grid lg:grid-cols-2 grid-cols-1 gap-6 mb-6">
        <Analysis
          name="Sản phẩm được bán"
          parameter={temporaryQuantity}
          color="rgb(0,170,85)"
          percent="+2.6%"
        />
        <Analysis
          name="Doanh Thu Tạm Tính"
          parameter={totalEstimatedPrice}
          color="rgb(0,184,217)"
          percent="-0.1%"
        />
      </div>
      <div className="grid lg:grid-cols-3 grid-cols-1 gap-6">
        <Analysis
          name="Sản phẩm được bán"
          parameter={countProd}
          color="rgb(0,170,85)"
          percent="+2.6%"
        />
        <Analysis
          name="Doanh Thu Thực"
          parameter={totalPrice}
          color="rgb(0,184,217)"
          percent="-0.1%"
        />
        <Analysis
          name="Lợi nhuận bán hàng"
          parameter={totalPrice / 2 - 500}
          color="rgb(248,167,2)"
          percent="+0.6%"
        />
      </div>
      <div className="grid grid-cols-3 gap-6 mt-6">
        <div className="col-span-3 lg:col-span-1">
          <MultipleRadialbars />
        </div>
        <div className="col-span-3 lg:col-span-2">
          <Area />
        </div>
      </div>
      <div className="text-lg flex justify-center py-6 font-bold">
        Sản phẩm bán Chạy
      </div>
      <div>
        <div className={`flex flex-col `}>
          <div className="-my-2 -mx-4 sm:-mx-6 lg:-mx-8">
            <div
              id="table-scroll"
              className="inline-block w-[94%] overflow-x-auto py-2 align-middle md:mx-6 lg:mx-8"
            >
              <div className="shadow ring-1 ring-black ring-opacity-5 md:rounded-lg overflow-x-auto">
                <table className="min-w-full">
                  <thead className="bg-[rgb(51,61,72)]">
                    <tr>
                      {columnProduct.map((column, index) => (
                        <th
                          key={index}
                          scope="col"
                          className={`py-3.5 pl-4 pr-3 w-fit text-left text-sm font-semibold ${
                            String(column).includes("Tên")
                              ? "text-white"
                              : "text-[rgb(145,158,171)]"
                          }`}
                        >
                          {column}
                        </th>
                      ))}
                    </tr>
                  </thead>
                  <tbody className="bg-[rgb(33,43,54)]">
                    {selling.map((item: any, index) => (
                      <tr key={index}>
                        <td
                          className={
                            "whitespace-nowrap min-w-[105px] w-fit px-3 py-4 text-sm bg-[rgb(33,43,54)] text-white"
                          }
                        >
                          {index + 1}
                        </td>
                        <td
                          className={
                            "whitespace-nowrap min-w-[105px] w-fit px-3 py-4 text-sm bg-[rgb(33,43,54)] text-white"
                          }
                        >
                          <div className="space-x-1 flex">
                            {item.productName}
                          </div>
                        </td>
                        <td
                          className={
                            "whitespace-nowrap min-w-[105px] w-fit px-3 py-4 text-sm bg-[rgb(33,43,54)] text-white"
                          }
                        >
                          {item.size}
                        </td>
                        <td
                          className={
                            "whitespace-nowrap min-w-[105px] w-fit px-3 py-4 text-sm bg-[rgb(33,43,54)] text-white"
                          }
                        >
                          {item.color}
                        </td>
                        <td
                          className={
                            "whitespace-nowrap min-w-[105px] w-fit px-3 py-4 text-sm bg-[rgb(33,43,54)] text-white"
                          }
                        >
                          <div className="ml-8">{item.quantitySold}</div>
                        </td>
                        <td
                          className={
                            "whitespace-nowrap min-w-[105px] w-fit px-3 py-4 text-sm bg-[rgb(33,43,54)] text-white"
                          }
                        >
                          {item.totalRevenue}
                        </td>
                        {/* <td>{item.productName}</td> */}
                      </tr>
                    ))}
                  </tbody>
                  {/* <tbody className=" bg-[rgb(33,43,54)]">
                    {selling.map((item, idx) => (
                      <td
                        className={`whitespace-nowrap min-w-[105px] w-fit px-3 py-4 text-sm bg-[rgb(33,43,54)] text-white ${
                          idx === 1 ? "font-semibold" : ""
                        }`}
                        key={idx}
                      >
                        <div className="flex space-x-1">{item}</div>
                      </td>
                    ))}
                  </tbody> */}
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Dashboard.getLayout = function getLayout(page: ReactElement) {
  return <MainAdmin>{page}</MainAdmin>;
};

export default Dashboard;
